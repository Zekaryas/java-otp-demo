package com.etechsc;

import com.bastiaanjansen.otp.HMACAlgorithm;
import com.bastiaanjansen.otp.SecretGenerator;
import com.bastiaanjansen.otp.TOTP;

import java.time.Duration;

/**
 * OTP generator
 */
public class OTPGen {
    private final TOTP totp;
    private static OTPGen instance;

    private OTPGen() {
        byte[] secret = SecretGenerator.generate();

        TOTP.Builder builder = new TOTP.Builder(secret);

        builder
                .withPasswordLength(8)
                .withAlgorithm(HMACAlgorithm.SHA1) // SHA256 and SHA512 are also supported
                .withPeriod(Duration.ofMinutes(3));

        this.totp = builder.build();
    }

    public static synchronized OTPGen getInstance() {
        if (instance == null) {
            instance = new OTPGen();
        }
        return instance;
    }

    public String getOTP() {
        return totp.now();
    }

    public boolean verifyOTP(String otpCode) {
        return totp.verify(otpCode);
    }

    public static void main(String[] args) {
        try {
            OTPGen app = OTPGen.getInstance();
            String code = app.getOTP();

            System.out.println(code);

            // To verify a token:
            boolean isValid = app.verifyOTP(code);
            System.out.println(isValid);
        } catch (IllegalStateException e) {
            // Handle error
        }
    }
}
