## Intro
A Java code for One time password(OTP) generator that uses a Time based algorithem.

## References
- [What is OTP](https://en.wikipedia.org/wiki/One-time_password)
- [What is Time based OTP(TOTP)](https://en.wikipedia.org/wiki/Time-based_One-Time_Password)

## Library used
- https://github.com/BastiaanJansen/OTP-Java
